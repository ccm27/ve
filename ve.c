#include <curses.h> /* libraries*/
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/*I put stuff to add later in a seperate file -it was better that way*/

#define MAXBUF 1000000 /*variables*/
char **ARGV;
char buf[MAXBUF];
int maxlines, maxcols;
int cursorx = 0;
int cursory = 0;
int linepos = 0; 
int buflen = 0;
int firstline = 0;
int colonmode = 0;
char colonchar;
int getcurpos();  
void display();
void foundaline(char *c);
void domove(char c);
void doinput();
void docolon();
void deletechar();
void deletechar();
void writefile();
void bottomline();
void initscrn();
void openfile();

int main(int argc, char* argv[]) {      /*main code*/
    ARGV = argv;
    initscrn();
    openfile();
    display(); 
    while (1) { /*event loop*/
        char c;
        c = getchar();
        if ((c == 'h') || (c == 'j') || (c == 'k') || (c =='l')){
            domove(c);
        } else if (c == 'i'){
            doinput();
        } else if (c == 'x'){
            deletechar();
        } else if (c == ':'){
            docolon();
        }
        display();
    }
}

/*functions:*/
void initscrn() {
    initscr();
    cbreak();
    noecho();
    clear();
    maxlines = LINES - 1;
    maxcols = COLS - 1;
}

void openfile() {
    FILE *fd = NULL;
    fd = fopen(ARGV[1],"r");
    if (fd != NULL){ //arguments
        size_t len;
        size_t max = MAXBUF;
        len = fread(buf,(size_t)1,max,fd);
        buf[len]=0;
        buflen = len+1;
        if (!len){
            mvaddstr(12, 10, "The file is empty");
            refresh();
            sleep(4);
        } 
    } else {
        mvaddstr(12, 10, "creating a new file"); /*put this on the bottom*/
        buf[0]='\n'; 
        buf[1]=0; 
        buflen = 2;
        refresh();
        sleep(4);
    }        
}
      
  

void display(){
    clear(); /*clear ncurses and begin counting lines*/
    linepos = 0;
    int i; 
    char sbuf[500];
    int spos = 0; 
    for (i = 0; i < buflen; i++){
    if (buf[i] == '\n'){
	    sbuf[spos++] = 0;
	    foundaline(sbuf);
	    spos = 0;
 	} 
       else {
            sbuf[spos++] = buf[i];
        }
    
    }
    
    // display each line with mvaddstr until at bottom
    // done
    /*and, here, draw everything on the screen*/
    //and here, addh
    bottomline();
    move(cursory,cursorx);
    refresh();
}
void bottomline(){
    char s[500];
    if (colonmode){
        sprintf(s,":%c",colonchar);
        mvaddstr(maxlines-1,0,s);
    } else {
        int cp = getcurpos();
        sprintf(s,"Howdy we are at character %d in %s which is %d chars long cx %d cy %d firstline %d",cp,ARGV[1],buflen,cursorx,cursory,firstline);
        mvaddstr(maxlines-1,0,s);
    }
}
void foundaline(char *c){
        int localposition = linepos - firstline;
        linepos++;
	if ((localposition >= 0) && (localposition < (maxlines-1))){
		mvaddstr(localposition,0,c);
		refresh();
	}
}
	void domove(char c){
            if (c == 'l'){
                cursorx++;
            } else if (c == 'h'){
                cursorx--;
            } else if (c == 'j'){ //down
                cursory++;
                if ((getcurpos() > buflen) || (getcurpos() < 0)){ //check for going past end of buffer
                    cursory--;
                } else if (cursory >= maxlines){ //check for going downwards at bottom screen
                    firstline++;
                    cursory--;
                }
            } else if (c == 'k'){
                cursory--; //add screen checks
                if (cursory < 0){
                    if (firstline > 0){
                        firstline--;
                    }
		    cursory++;
                }
            }
             
	}
void doinput(){
    char c = getchar();
    cursorx++;
    while (c != 27){  //keep reading until ESC (27)
        if ((c != KEY_DL) && (c != KEY_DC) && (c != KEY_BACKSPACE)) { //Backspace
            if (c == '\r'){
                c = '\n';
            }
	    //find current position in buffer of cursor
	    int curpos;
	    curpos = getcurpos()-1; //want insert not delete
	    //find strlen() of rest of buffer.
	    char *restofbuf = &buf[curpos];
	    int lenrest = strlen(restofbuf)+1;
	    //use memmove() to move everything down one byte.
	    memmove(restofbuf+1,restofbuf,(size_t)lenrest);
	    *restofbuf = c;
	    buflen++;
            if (c != '\n'){
	        cursorx++;
            } else {
                cursory++;
                cursorx = 0;
            }
	    display();
	    c = getchar();
        } else {
            deletechar();
            cursorx--;
            display();
            c = getchar();
        }
    }
    cursorx--; //backtrack after escape
}

int getcurpos(){
    int chpos = 0;
    int skip = cursory + firstline;
    char *curbuf = buf;
    while (skip > 0){
        char *tbuf = index(curbuf,'\n');
        int thislinelen = tbuf - curbuf + 1;
        chpos += thislinelen;
        curbuf += thislinelen;
        skip--;
    }
    return chpos + cursorx;
    //count line cursor is on in the arry
	//figure how to count char in string(line)
}
void deletechar()
{
    //find current position in buffer of cursor
    int curpos;
    curpos = getcurpos();
    //find strlen() of rest of buffer.
    char *restofbuf = &buf[curpos];
    int lenrest = strlen(restofbuf+1);
    //use memmove() to move everything down one byte.
    memmove(restofbuf,restofbuf+1,(size_t)lenrest);
    buflen--;
// *insert dd too
    //""
}
void deleteline(){
}
void docolon(){
    char c;
    colonmode = 1;
    colonchar = ' ';
    display();
    c = getchar();
    colonchar = c;
    display();
    sleep(1);
    if (c == 'w'){
        writefile();
    } else if (c == 'q'){
        endwin();
        exit(0);
    } else {
         //huh?
    }
    colonmode = 0;
}
void writefile(){
        FILE *fd;
	fd = fopen(ARGV[1],"w");
	if (fd != NULL){
	    size_t len = buflen;
	    size_t lenw = 0;
	    len = fwrite(buf,(size_t)1,len,fd);
	}
}
